import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, MinLength } from "class-validator";

export class CreateRoleDto {

    @ApiProperty()
    @IsNotEmpty()
    @MinLength(3)
    role: string;

    @ApiProperty()
    @MinLength(6)
    status_role: string
}
