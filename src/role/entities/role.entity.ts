import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: 'role'})
export class Role {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    role: string

    @Column()
    status_role: string
}
