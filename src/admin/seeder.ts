import { TypeOrmModule } from "@nestjs/typeorm";
import {seeder} from "nestjs-seeder";
import { Admin } from "./entities/admin.entity";
import { AdminSeeder } from "./admin.seeder";

seeder({
    imports:[
        TypeOrmModule.forRoot({
            name: 'default',
            type: 'mysql',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_DB,
            synchronize: true,
            dropSchema: false,
            logging: true,
            entities: ['dist/**/*.entity.js'],
        })
    ]
}).run([AdminSeeder])