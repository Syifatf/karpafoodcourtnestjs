import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './entities/admin.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { v4 as uuidv4 } from 'uuid';
import * as bcrypt from 'bcrypt';
import { FindAdminDto } from './dto/find-admin.dto';
import { FilterOperator, FilterSuffix, Paginate, PaginateQuery, paginate, Paginated } from 'nestjs-paginate'
@Injectable()
export class AdminService {

  // new
  constructor(
    @InjectRepository(Admin) private readonly adminRepository: Repository<Admin>
  ) {}

  // create(createAdminDto: CreateAdminDto) {
  //   return 'This action adds a new admin';
  // }

  public findAll(query: PaginateQuery): Promise<Paginated<Admin>> {
    return paginate(query, this.adminRepository, {
      sortableColumns: ['username', 'email', 'role'],
      // nullSort: 'last',
      defaultSortBy: [['username', 'DESC']],
      searchableColumns: ['username', 'email', 'role'],
      select: ['id', 'username', 'email', 'role', 'created_by'],
      filterableColumns: {
        username: [FilterOperator.EQ, FilterSuffix.NOT],
        // role: true,
      },
      // filter: query.filter,
    })
  }

  //  new untuk pagination
  async all(): Promise<Admin[]> {
    return this.adminRepository.find();
  }

  //  untuk get data pagination
  async getPagination(alias: string) {
    return this.adminRepository.createQueryBuilder(alias);
  }


  async getByEmail(email: string) {
    const user = await this.adminRepository.findOne({ where: {email} });
    if (user) {
      return user;
    }
    throw new HttpException('User with this email does not exist', HttpStatus.NOT_FOUND);
  }

  //  create new
  async createAdmin(createAdminDto: CreateAdminDto): Promise<Admin> {

    const saltOrRounds = 10;
    const password = createAdminDto.password
    const hash = await bcrypt.hash(password, saltOrRounds )

    const admin: Admin = new Admin();

    admin.id = uuidv4();
    admin.username = createAdminDto.username;
    admin.role = createAdminDto.role;
    admin.email = createAdminDto.email;
    admin.password = hash;
    admin.created_by = createAdminDto.created_by;

    return this.adminRepository.save(admin)
  }

  // const newUser = await this.usersRepository.create(userData);
  //   await this.usersRepository.save(newUser);
  //   return newUser;


  // ===========================================
  // async findAll(): Promise<Admin[]> {
  //   // return `This action returns all admin`;
  //   return this.adminRepository.find();
  // }

  async findOne(id: string): Promise<Admin> {
    const admin = await this.adminRepository.findOne({ where: {id}});
    // this.findOne({where : {username: id}});
    
    if (!admin) {
      throw new NotFoundException(`Admin with ID ${id} not found`);
    }

    return admin;
    // return this.adminRepository.findOneBy({ id });
  }

  async update(id: string, updateAdminDto: UpdateAdminDto) {

    const admin = await this.findOne(id);
    // return `This action updates a #${id} admin`;
    // const admin: Admin = new Admin();
    admin.username = updateAdminDto.username;
    admin.role = updateAdminDto.role;
    admin.email = updateAdminDto.email;
    admin.password = updateAdminDto.password;
    admin.created_by = updateAdminDto.created_by;
    
    return this.adminRepository.save(admin)
  }

  // remove(id: number): Promise<{ affected?: number }> {
  //   // return `This action removes a #${id} admin`;
  //   return this.adminRepository.delete(id)
  // }

  async remove(id: string): Promise<{ affected?: number }> {
    const result = await this.adminRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Admin with ID ${id} not found`);
    }

    return result;
  }

  // async findAll(dto: FindAdminDto) {
  //   const { username, email, role} = dto ;
  //   const conditions: FindOptionsWhere<Admin> | FindOptionsWhere<Admin>[] = {
  //     ...(username ? { username } : {}),
  //     ...(role ? { role } : {}),
  //     ...(email ? { email } : {}),
  //   }

  //   return await this.adminRepository.find({
  //     where: conditions,
  //   });
  // }

  //  ganti password
  // async updatePassword(email: string, newPassword: string, reNewPassword: string, updateAdminDto: UpdateAdminDto) {
  //   try {
  //     const akunAdmin = await this.adminRepository.findOne({ where: {email}});

  //     // akunAdmin.password = updateAdminDto.password;
  //     if(!akunAdmin){
  //       throw new NotFoundException(`Admin with email ${email} not found`);
  //     }

  //     const isPasswordMatching = await bcrypt.compare(
  //       reNewPassword,
  //       newPassword
  //     );

  //     if (!isPasswordMatching) {
  //       throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
  //     }
  //     reNewPassword = updateAdminDto.password;
  //     this.adminRepository.save(akunAdmin)
  //     return akunAdmin;
  //   } catch (error) {
  //     throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
  //   }
  // }
}
