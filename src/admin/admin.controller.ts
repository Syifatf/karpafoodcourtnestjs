import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Req } from '@nestjs/common';
import { AdminService } from './admin.service';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { FilterOperator, FilterSuffix, Paginate, PaginateQuery, paginate, Paginated } from 'nestjs-paginate'
import { v4 as uuidv4 } from 'uuid';
import { query, Request } from 'express';


@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Post()
  create(@Body() createAdminDto: CreateAdminDto) {
    return this.adminService.createAdmin(createAdminDto);
  }

  // @Get()
  // findMany(@Query() query: FindAdminDto) {
  //   return this.adminService.findAll(query);
  // }

  // @Get()
  // findAll() {
  //   return this.adminService.findAll();
  // }

  // @Get()
  // findAll() {
  //   return this.adminService.findAll();
  // }
  
  // @Get()
  // public findAll(@Paginate() query: PaginateQuery): Promise<Paginated<Admin>> {
  //   return this.adminService.findAll(query)
  // }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.adminService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAdminDto: UpdateAdminDto) {
    return this.adminService.update(id, updateAdminDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.adminService.remove(id);
  }

  @Get()
  async bePagination(@Req() req: Request){
    const builder = await this.adminService.getPagination('admin');
    const page: number = parseInt(req.query.page as any) || 1;
    const size: number = parseInt(req.query.size as any) || 5;
    const data_amount = await builder.getCount();

    if(req.query.search){
      builder.where("(admin.username LIKE :search OR admin.email LIKE :search OR admin.created_by LIKE :search)", {search: `%${req.query.search}%`})
    }

    const sort: any = req.query.sort;
    const orderby: any = req.query.order || 'asc';
    if(sort){
      // builder.orderBy('admin.username', "DESC")

      const sortableColumns = ["username", "email", "created_by", "role"];
      if (sortableColumns.includes(sort)){
        builder.orderBy(`admin.${sort}`, orderby.toUpperCase() as "ASC" | "DESC")
      }
    }

    //  filter
    for (const columnName in req.query) {
      if (columnName !== 'search' && columnName !== 'sort' && columnName !== 'order' && columnName !== 'page' && columnName !== 'size') {
        const filterValue = req.query[columnName];
        builder.andWhere(`admin.${columnName} = :${columnName}`, { [columnName]: filterValue });
      }
    }

    builder.offset((page - 1) * size).limit(size)
    return {
      data: await builder.getMany(),
      data_amount,
      page,
      size,
      last_page: Math.ceil(data_amount / size)
    }
  }
}
