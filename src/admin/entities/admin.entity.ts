import { Role } from "src/role/entities/role.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Admin {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    username: string;

    @Column({type: 'varchar'})
    password: string;

    @Column()
    role: string;

    // unique: true
    @Column()
    email: string;

    @Column({ default: () => 'CURRENT_TIMESTAMP' })
    created_at: Date;

    // @UpdateDateColumn()
    @Column({type:'varchar', length: 25})
    created_by: string;

    // @ManyToMany(() => Role, {
    //     cascade: true,
    // })
    // status_role: Role[];

    // @ManyToOne((type) => User, (user) => user.photos)
    // user: User
}
