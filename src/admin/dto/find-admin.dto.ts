export class FindAdminDto {
    username: string;
    role: string;
    email: string;
    search: string;
}