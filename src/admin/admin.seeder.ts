import { InjectRepository } from "@nestjs/typeorm";
import { Seeder, DataFactory } from "nestjs-seeder";
import { Admin } from "./entities/admin.entity";
import { Repository } from "typeorm";

export class AdminSeeder implements Seeder {
    constructor(
        @InjectRepository(Admin) private readonly adminRepository: Repository<Admin>){}

    drop(): Promise<any> {
        return this.adminRepository.delete({})
    }

    seed(): Promise<any> {
        const admin = DataFactory.createForClass(Admin).generate(50)
        return Promise.resolve(undefined)
    }
}